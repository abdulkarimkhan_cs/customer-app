//
//  Profile.swift
//  3CLICK
//
//  Created by Shakeel Khan on 6/27/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper
import PhoneNumberKit
import DropDown

class Profile: BaseController {
    
    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var imgProfileBackground: UIImageView!
    @IBOutlet weak var tfEmail: UITextFieldDynamicFonts!
    @IBOutlet weak var tfName: UITextFieldDynamicFonts!
    @IBOutlet weak var tfState: UITextFieldDynamicFonts!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var tfCountryCode: UITextFieldDynamicFonts!
    @IBOutlet weak var tfPhoneNumber: UITextFieldDynamicFonts!
    
    var pickedImage: UIImage?
    var image_url = ""
    
    let stateDropDown = DropDown()
    var arrStates = [CityModel]()
    var selectedState: CityModel?
    
    var shouldVerfiyPin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.PROFILE.text
        self.setData()
        self.getStates()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnImagePicker(_ sender: UIButton){
        self.chooseImageUploadType()
    }
    @IBAction func onBtnCountryCode(_ sender: UIButton) {
        self.openCountryPicker()
    }
    @IBAction func onBtnUpdate(_ sender: BlueGradient) {
        self.validate(sender: sender)
    }
}
//MARK:- Helper Methods
extension Profile{
    private func setData(){
        let user = AppStateManager.sharedInstance.loggedInUser
        
        if let imgURL = URL(string: user?.profilePic ?? ""){
            self.imgProfile.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "galleryPlaceholder"), options: .highPriority, completed: nil)
            self.imgProfileBackground.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "galleryPlaceholder"), options: .highPriority, completed: nil)
        }
        
        self.tfEmail.text = user?.email ?? ""
        self.tfName.text = user?.firstName ?? ""
        self.tfState.text = user?.city?.name ?? ""
        
        let selectedState = CityModel()
        selectedState.id = user?.city?.id
        selectedState.name = user?.city?.name ?? ""
        self.selectedState = selectedState
        
        self.setStatesDropDown()
        
        if let phoneNumber = user?.contact, !phoneNumber.isEmpty{
            let phoneNumberKit = PhoneNumberKit()
            do {
                let phoneNum = try phoneNumberKit.parse(phoneNumber)
                let regionCode = phoneNumberKit.getRegionCode(of: phoneNum)
                self.imgCountryFlag.image = Utility.emojiFlag(regionCode: regionCode ?? "PK")?.image()
                let countryCode = try phoneNumberKit.parse(phoneNumber).countryCode
                let code = "+\(countryCode)"
                self.tfCountryCode.text = code
                self.tfPhoneNumber.text = phoneNumber.replacingOccurrences(of: "\(code)", with: "")
            }
            catch {
                print("Generic parser error")
            }
        }
        else{
            self.setPhoneNumberUI()
        }
    }
    private func validate(sender:UIButton){
        let number = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        do {
            let _ = try self.tfEmail.validatedText(validationType: ValidatorType.email)
            let _ = try self.tfName.validatedText(validationType: ValidatorType.username)
            let _ = try self.tfState.validatedText(validationType: ValidatorType.state)
            if !Utility.validatePhoneNumber(number: number){
                sender.shake()
                Utility.main.showToast(message: Strings.INVALID_PHONE.text)
                return
            }
            self.verifyUpdatedData()
            self.uploadProfileImage()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
    private func chooseImageUploadType(){
        let alert = UIAlertController(title: "Upload profile photo" , message: "How do you want to set your profile picture?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    private func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    private func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }
    }
    private func setPhoneNumberUI(){
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: "AU")?.image()
        self.tfCountryCode.text = "+61"
    }
    private func openCountryPicker(){
        let controller = MICountryPicker()
        controller.delegate = self
        controller.showCallingCodes = true
        //controller.setStatusBarStyle(.lightContent)
        self.present(controller, animated: true, completion: nil)
    }
    private func setStatesDropDown(){
        self.stateDropDown.dataSource = self.arrStates.map{$0.name ?? ""}
        self.stateDropDown.direction = .any
        self.stateDropDown.anchorView = self.tfState
        self.stateDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfState.text = item
            self.selectedState = self.arrStates[index]
        }
    }
    private func verifyUpdatedData(){
        let user = AppStateManager.sharedInstance.loggedInUser
        let email = user?.email ?? ""
        let mobileNumber = user?.contact ?? ""
        
        let newEmail = self.tfEmail.text ?? ""
        let newMobileNumber = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        
        if email != newEmail || mobileNumber != newMobileNumber{
            self.shouldVerfiyPin = true
        }
    }
}
//MARK: - Image Picker
extension Profile: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.pickedImage = pickedImage
            self.imgProfile.image = self.pickedImage
            self.imgProfileBackground.image = self.pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- MICountryPickerDelegate
extension Profile: MICountryPickerDelegate{
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        self.dismiss(animated: true, completion: nil)
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: code)?.image()
    }
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.tfCountryCode.text = dialCode
    }
}
//MARK:- UITextFieldDelegate
extension Profile: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfState{
            self.stateDropDown.show()
            return false
        }
        return true
    }
}
//MARK:- Services
extension Profile{
    private func getStates() {
        print("Check")
        APIManager.sharedInstance.usersAPIManager.GetStates(params: [:], success: { (responseObject) in
            guard let response = responseObject as? [[String:Any]] else {return}
            print(response)
            self.arrStates = Mapper<CityModel>().mapArray(JSONArray: response)
            self.setStatesDropDown()
        }) { (error) in
            print(error)
        }
    }
    private func uploadProfileImage(){
        if let pickedImage = self.pickedImage{
            let image = pickedImage.jpegData(compressionQuality: 0.01) ?? Data()
            let param:[String:Any] = ["image":image]
            APIManager.sharedInstance.usersAPIManager.UploadFile(params: param, success: { (responseObject) in
                print(responseObject)
                let response = responseObject as Dictionary
                if let url = response["url"] as? String{
                    self.image_url = url
                }
                self.updateUserProfile()
            }) { (error) in
                print(error)
            }
        }
        else{
            self.updateUserProfile()
        }
    }
    private func updateUserProfile(){
        //let email = self.tfEmail.text ?? ""
        let firstName = self.tfName.text ?? ""
        let city = self.selectedState?.id ?? "0"
        //let mobile_number = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        var params:[String:Any] = ["firstName":firstName,
                                   "city":city]
        
        if !self.image_url.isEmpty{
            params["profilePic"] = image_url
        }
        
        print(params)
        APIManager.sharedInstance.usersAPIManager.UpdateProfile(params: params, success: { (responseObject) in
            print(responseObject)
//            if self.shouldVerfiyPin{
//                super.pushToCodeVerification()
//            }
//            else{
                Utility.main.showAlert(message: Strings.PROFILE_UPDATED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler:{
                    guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
                    AppStateManager.sharedInstance.loginUser(user: user)
                })
//            }
        }) { (error) in
            print(error)
        }
    }
}
