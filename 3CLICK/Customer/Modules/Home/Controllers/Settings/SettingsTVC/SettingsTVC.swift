//
//  SettingsTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 23/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

struct SettingsData {
    var image:UIImage?
    var option:String
    var placeHolder:String
    var interaction:Bool
}

class SettingsTVC: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var tfOption: UITextField!
    
    func setData(data:SettingsData){
        self.imgIcon.image = data.image
        self.tfOption.text = data.option
        self.tfOption.isUserInteractionEnabled = data.interaction
    }
}
