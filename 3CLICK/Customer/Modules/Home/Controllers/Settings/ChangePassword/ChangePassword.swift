    //
    //  ChangePassword.swift
    //  3CLICK
    //
    //  Created by Sierra-PC on 28/06/2019.
    //  Copyright © 2019 Sierra-PC. All rights reserved.
    //
    
    import UIKit
    
    class ChangePassword: BaseController {
        
        @IBOutlet weak var tfOldPassword: UITextField!
        @IBOutlet weak var tfNewPassword: UITextField!
        @IBOutlet weak var tfConfirmPassword: UITextField!
        @IBOutlet weak var btnBack: UIButton!
        @IBOutlet weak var lblChangePassword: UILableDynamicFonts!
        @IBOutlet weak var lblDescription: UILableDynamicFonts!
        @IBOutlet weak var lblOldPassword: UILableDynamicFonts!
        @IBOutlet weak var btnChangePassword: OrangeGradient!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.setUI()
        }
        
        @IBAction func onBtnBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
        @IBAction func onBtnShowPassword(_ sender: UIButton) {
            sender.isSelected = !sender.isSelected
            switch sender.tag {
            case 0:
                self.tfOldPassword.isSecureTextEntry = !self.tfOldPassword.isSecureTextEntry
            case 1:
                self.tfNewPassword.isSecureTextEntry = !self.tfNewPassword.isSecureTextEntry
            case 2:
                self.tfConfirmPassword.isSecureTextEntry = !self.tfConfirmPassword.isSecureTextEntry
            default:
                break
            }
        }
        @IBAction func onBtnChangePassword(_ sender: OrangeGradient) {
            self.validate(sender: sender)
        }
    }
    //MARK:- Helper Methods
    extension ChangePassword{
        private func setUI(){
            if !AppStateManager.sharedInstance.isUserLoggedIn(){
                self.title = Strings.FORGOT_PASSWORD.text
                self.lblChangePassword.text = Strings.FORGOT_PASSWORD.text
                self.lblDescription.text = Strings.ENTER_EMAIL_PIN.text
                self.lblOldPassword.text = Strings.EMAIL_OTP.text
                self.btnChangePassword.setTitle(Strings.CHANGE_PASSWORD.text.uppercased(), for: .normal)
            }
            else{
                self.btnBack.isHidden = true
                self.title = Strings.CHANGE_PASSWORD.text
            }
        }
        private func validate(sender:UIButton){
            let newPassword = self.tfNewPassword.text ?? ""
            let confirmPassword = self.tfConfirmPassword.text ?? ""
            do {
                if AppStateManager.sharedInstance.isUserLoggedIn(){
                    let _ = try self.tfOldPassword.validatedText(validationType: ValidatorType.requiredField(field: Strings.OLD_PASSWORD.text))
                }
                else{
                    let _ = try self.tfOldPassword.validatedText(validationType: ValidatorType.requiredField(field: Strings.EMAIL_OTP.text))
                }
                let _ = try self.tfNewPassword.validatedText(validationType: ValidatorType.requiredField(field: Strings.NEW_PASSWORD.text))
                let _ = try self.tfConfirmPassword.validatedText(validationType: ValidatorType.requiredField(field: Strings.CONFIRM_PASSWORD.text))
                if newPassword != confirmPassword{
                    throw ValidationError(Strings.PWD_DONT_MATCH.text)
                }
                self.changePassword()
            } catch(let error) {
                sender.shake()
                Utility.main.showToast(message: (error as! ValidationError).message)
            }
        }
    }
    //MARK:- Helper Methods
    extension ChangePassword{
        private func changePassword(){
            
            let old_password = self.tfOldPassword.text ?? ""
            let new_password = self.tfNewPassword.text ?? ""
            let confirm_password = self.tfConfirmPassword.text ?? ""
            if AppStateManager.sharedInstance.isUserLoggedIn(){
                let params:[String:Any] = ["oldPassword":old_password,
                                           "newPassword":new_password]
                APIManager.sharedInstance.usersAPIManager.ChangePassword(params: params, success: { (responseObject) in
                    Utility.main.showAlert(message: Strings.PASSWORD_CHANGED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler: {
                        AppStateManager.sharedInstance.deleteLoggedInUser()
                        AppDelegate.shared.changeRootViewController()
                    })
                }) { (error) in
                    print(error)
                }
            }
            else{
                let params:[String:Any] = ["code":old_password,
                                           "password":new_password,
                                           "confirmPassword":confirm_password]
                APIManager.sharedInstance.usersAPIManager.ResetPassword(params: params, success: { (responseObject) in
                    Utility.main.showAlert(message: Strings.PASSWORD_CHANGED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler: {
                        AppDelegate.shared.changeRootViewController()
                    })
                }) { (error) in
                    print(error)
                }
            }
        }
    }
