//
//  CustomMarker.swift
//  3CLICK
//
//  Created by Shakeel Khan on 10/23/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

enum MarkerPinType{
    case pickUp
    case dropOff
}

class CustomMarker: UIView {

    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var imgMarker: UIImageView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomMarker", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

    func setData(address:String,pin:MarkerPinType){
        self.lblPlace.text = address
        switch pin{
        case .pickUp:
            self.imgMarker.image = UIImage(named: "pickUpPin")
        case .dropOff:
            self.imgMarker.image = UIImage(named: "dropOffPin")
        }
    }
}
