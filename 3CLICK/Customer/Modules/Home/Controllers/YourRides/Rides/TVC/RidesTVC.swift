//
//  RidesTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 21/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import HGRippleRadarView

class RidesTVC: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewRipple: RippleView!
    
    func setData(data:Bookings){
        let date = data.deliveryDate ?? "2019-06-19T17:25:59.000Z"
        let formatedDate = Utility.getFormattedDate(date: date, format: "dd MMM yy, EEE h:mm a")
        self.lblDate.text = formatedDate
        
        let amount = "\(Strings.CURRENCY.text) \(Int(data.amount))"
        self.lblAmount.text = amount
        
        self.lblSource.text = data.pickUpLocation ?? "Pickup"
        self.lblDestination.text = data.dropOffLocation ?? "Dropoff"
        self.lblStatus.text = data.bookingStatus ?? "-"
        
        if (data.bookingStatus ?? "") == BookingType.inProgress.rawValue{
            self.viewRipple.isHidden = false
        }
        else{
            self.viewRipple.isHidden = true
        }
        
//        switch data.rideStatusId{
//        case CurrentRideStatus.approved.rawValue:
//            self.viewRipple.isHidden = true
//        case CurrentRideStatus.inProgress.rawValue:
//            self.viewRipple.isHidden = false
//        case CurrentRideStatus.rejected.rawValue:
//            self.viewRipple.isHidden = true
//        case CurrentRideStatus.pending.rawValue:
//            self.viewRipple.isHidden = true
//        case CurrentRideStatus.completed.rawValue:
//            self.viewRipple.isHidden = true
//        case CurrentRideStatus.cancel.rawValue:
//            self.viewRipple.isHidden = true
//        default:
//            self.viewRipple.isHidden = true
//        }
    }
//    func setData(type:BookingType){
//        switch type{
//        case .current:
//            self.viewRipple.isHidden = false
//            self.lblStatus.text = "In transit"
//        case .pending:
//            self.viewRipple.isHidden = true
//            self.lblStatus.text = "Pending"
//        case .finished:
//            self.viewRipple.isHidden = true
//            self.lblStatus.text = "Completed"
//        }
//    }
}

