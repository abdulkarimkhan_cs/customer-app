//
//  Home.swift
//  3CLICK
//
//  Created by Sierra-PC on 27/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import DropDown
import Alamofire
import SwiftyJSON
import ObjectMapper
import RealmSwift

struct HireFleetData {
    var weight = ""
    var pickUpLocation = ""
    var pickUpLatitude = ""
    var pickUpLongitude = ""
    var dropOffLocation = ""
    var dropOffLatitude = ""
    var dropOffLongitude = ""
    var pickUpCity = ""
    var dropOffCity = ""
    var pickUpDate = ""
    var vendor = ""
    var commodity = ""
    var truckType = ""
    var truckSize = ""
}

enum LocationPickerType{
    case pickUp
    case dropOff
}

class Home: BaseController {
    
    @IBOutlet weak var lblPickUpTitle: UILabel!
    @IBOutlet weak var lblDropOffTitle: UILabel!
    @IBOutlet weak var lblPickUp: UILabel!
    @IBOutlet weak var lblDropOff: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnPickUpTick: UIButton!
    @IBOutlet weak var btnDropOffTick: UIButton!
    @IBOutlet weak var imgPickUpMarker: UIImageView!
    @IBOutlet weak var tfCommodity: UITextField!
    @IBOutlet weak var tfTruckType: UITextField!
    @IBOutlet weak var tfTruckSize: UITextField!
    @IBOutlet weak var tfWeightInTons: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    
    var locationPickerType = LocationPickerType.pickUp
    lazy var geocoder = CLGeocoder()
    
    let dropDown = DropDown()
    var arrWeights = List<AttributeModel>()
    var selectedWeight: AttributeModel?
    
    let commodityDropDown = DropDown()
    var arrCommodity = List<AttributeModel>()
    var selectedCommodity: AttributeModel?
    
    let truckTypeDropDown = DropDown()
    var arrTruckType = List<AttributeModel>()
    var selectedTruckType: AttributeModel?
    
    let truckSizeDropDown = DropDown()
    var arrTruckSize = ["20 ft","40 ft","45 ft","50 ft"]
    var selectedTruckSize: String?
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    var pickUpLocation : CLLocationCoordinate2D?
    var dropOffLocation: CLLocationCoordinate2D?
    var placesClient: GMSPlacesClient!
    var pickUpCity: String?
    var dropOffCity: String?
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    var pickUpLocationMarker = GMSMarker()
    var dropOffLocationMarker = GMSMarker()
    var minDate: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .day, value: 0, to: Date(), options: [])!//10
    }
    var maxDate: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .day, value: 70, to: Date(), options: [])!
    }
    var selectedDate:Date?
    var flagShouldReverseGeoCode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.setGMSMapView()
        self.placesClient = GMSPlacesClient.shared()
        self.getAllAttributes()
        //self.setDate(date: Date())
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setLocationManager()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func onBtnPickUpTick(_ sender: UIButton) {
        if self.btnPickUpTick.isSelected{return}
        self.btnPickUpTick.isSelected = true
        self.btnDropOffTick.isSelected = false
        self.locationPickerType = .pickUp
        self.showPicker()
    }
    @IBAction func onBtnDropOffTick(_ sender: UIButton) {
        if self.btnDropOffTick.isSelected{return}
        self.btnDropOffTick.isSelected = true
        self.btnPickUpTick.isSelected = false
        self.locationPickerType = .dropOff
        self.showPicker()
    }
    @IBAction func onBtnPickUpLocationPicker(_ sender: UIButton) {
        self.locationPickerType = .pickUp
        self.pickLocationFromPlacePicker()
    }
    @IBAction func onBtnDropOffLocationPicker(_ sender: UIButton) {
        self.locationPickerType = .dropOff
        self.pickLocationFromPlacePicker()
    }
    @IBAction func onBtnGoToMyLocation(_ sender: UIButton) {
        if self.currentLocation == nil{
            Utility.openSettings()
            return
        }
        if self.pickUpLocation != nil && self.dropOffLocation != nil{
            self.fitAllMarkersBounds()
        }
        else{
            self.zoomToSearchLocation(location: self.currentLocation ?? CLLocationCoordinate2D())
        }
    }
    @IBAction func onTfDatePicker(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
    @IBAction func onBtnHire(_ sender: BlueGradient) {
        self.validate(sender: sender)
    }
    @IBAction func onBtnDatePicker(_ sender: Any) {
        Utility.main.showAlertNowLater(message: Strings.HIRE_NOW_LATER.text, title: Strings.ALERT.text, controller: self) { (Later, Now) in
            if Now != nil{
                self.setDate(date: Date())
                self.tfDate.delegate = nil
            }
            else{
                self.tfDate.delegate = self
                self.tfDate.becomeFirstResponder()
            }
        }
    }
}
//MARK:- Helper Methods
extension Home{
    private func setGMSMapView(){
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        //self.mapView.settings.myLocationButton = true
        //self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 6)
    }
    private func setDropDowns(){
        self.setCommodityDropDownUI()
        self.setTruckTypeDropDownUI()
        self.setTruckSizeDropDownUI()
        self.setWeightDropDownUI()
    }
    private func setCommodityDropDownUI(){
        self.commodityDropDown.dataSource = self.arrCommodity.map{$0.name ?? ""}
        self.commodityDropDown.direction = .top
        self.commodityDropDown.anchorView = self.tfCommodity
        self.commodityDropDown.width = self.tfCommodity.frame.width
        self.commodityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfCommodity.text = item
            self.selectedCommodity = self.arrCommodity[index]
        }
    }
    private func setTruckTypeDropDownUI(){
        self.truckTypeDropDown.dataSource = self.arrTruckType.map{$0.name ?? ""}
        self.truckTypeDropDown.direction = .top
        self.truckTypeDropDown.anchorView = self.tfTruckType
        self.truckTypeDropDown.width = self.tfTruckType.frame.width
        self.truckTypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfTruckType.text = item
            self.selectedTruckType = self.arrTruckType[index]
        }
    }
    private func setTruckSizeDropDownUI(){
        self.truckSizeDropDown.dataSource = self.arrTruckSize
        self.truckSizeDropDown.direction = .top
        self.truckSizeDropDown.anchorView = self.tfTruckSize
        self.truckSizeDropDown.width = self.tfTruckSize.frame.width
        self.truckSizeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfTruckSize.text = item
            self.selectedTruckSize = self.arrTruckSize[index]
        }
    }
    private func setWeightDropDownUI(){
        self.dropDown.dataSource = self.arrWeights.map{$0.name ?? ""}
        self.dropDown.direction = .top
        self.dropDown.anchorView = self.tfWeightInTons
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfWeightInTons.text = item
            self.selectedWeight = self.arrWeights[index]
        }
    }
    private func setLocationManager(){
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    private func zoomToSearchLocation(location: CLLocationCoordinate2D){
        CATransaction.begin()
        CATransaction.setValue(0.5, forKey: kCATransactionAnimationDuration)
        let fancy = GMSCameraPosition.camera(withLatitude: location.latitude,
                                             longitude: location.longitude,
                                             zoom: 18,
                                             bearing: 0,
                                             viewingAngle: 0)
        self.mapView.animate(to: fancy)
        CATransaction.commit()
    }
    private func hidePicker(){
        self.btnPickUpTick.isSelected = false
        self.btnDropOffTick.isSelected = false
        self.imgPickUpMarker.isHidden = true
    }
    private func showPicker(){
        self.imgPickUpMarker.isHidden = false
        switch self.locationPickerType{
        case .pickUp:
            self.btnPickUpTick.isSelected = true
            self.btnDropOffTick.isSelected = false
        case .dropOff:
            self.btnPickUpTick.isSelected = false
            self.btnDropOffTick.isSelected = true
        }
    }
    private func fitAllMarkersBounds() {
        var bounds = GMSCoordinateBounds()
        var markerList = [GMSMarker]()
        if let pathBounds = self.getPathBounds(){
            markerList = pathBounds
        }
        markerList.append(self.pickUpLocationMarker)
        markerList.append(self.dropOffLocationMarker)
        for marker in markerList {
            bounds = bounds.includingCoordinate(marker.position)
        }
        print(markerList)
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(160))
        self.mapView.animate(with: update)
    }
    private func getPathBounds()->[GMSMarker]?{
        if self.path.count() == 0{return nil}
        var arrPinPoints = [GMSMarker]()
        let pathTaken = self.path.count()
        
        if pathTaken > 0{
            for i in 0..<pathTaken{
                let point = path.coordinate(at: i)
                let position = CLLocationCoordinate2D(latitude: point.latitude , longitude: point.longitude)
                arrPinPoints.append(GMSMarker(position: position))
            }
        }
        return arrPinPoints
    }
    private func pickLocationFromPlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        //        // Specify the place data types to return.
        //        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
        //            UInt(GMSPlaceField.placeID.rawValue))!
        //        autocompleteController.placeFields = fields
        //        // Specify a filter.
        //        let filter = GMSAutocompleteFilter()
        //        filter.type = .address
        //        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        self.present(autocompleteController, animated: true, completion: nil)
    }
    private func setPickedAddressFromPlacePickerVC(_ place:GMSPlace){
        switch self.locationPickerType{
        case .pickUp:
            self.lblPickUpTitle.text = place.name ?? "Pick-up Location"
            self.lblPickUp.text = place.formattedAddress ?? "Location Area - City"
            self.pickUpLocation = place.coordinate
            self.pickUpCity = place.addressComponents?.first?.name ?? ""
            self.hidePicker()
            self.zoomToSearchLocation(location: self.pickUpLocation ?? CLLocationCoordinate2D())
            self.addPickUpMarker()
            if let _ = self.dropOffLocation{
                self.hidePicker()
                self.getRoute()
            }
        case .dropOff:
            self.lblDropOffTitle.text = place.name ?? "Drop-off Location"
            self.lblDropOff.text = place.formattedAddress ?? "Location Area - City"
            self.dropOffLocation = place.coordinate
            self.dropOffCity = place.addressComponents?.first?.name ?? ""
            self.addDropOffMarker()
            self.hidePicker()
            self.getRoute()
        }
    }
    private func getHireFleetData()->HireFleetData{
        
        let weight = self.selectedWeight?.name ?? ""
        let pickUpLocation = self.lblPickUp.text ?? ""
        let pickUpLatitude = "\(self.pickUpLocation?.latitude ?? 0)"
        let pickUpLongitude = "\(self.pickUpLocation?.longitude ?? 0)"
        let dropOffLocation = self.lblDropOff.text ?? ""
        let dropOffLatitude = "\(self.dropOffLocation?.latitude ?? 0)"
        let dropOffLongitude = "\(self.dropOffLocation?.longitude ?? 0)"
        let pickUpCity = self.pickUpCity ?? ""
        let dropOffCity = self.dropOffCity ?? ""
        let pickUpDate = Utility.getFormattedDate(date: self.selectedDate ?? self.minDate, format: "yyyy-MM-dd HH:mm:ss")
        let vendor = ""
        let commodity = self.selectedCommodity?.id ?? ""
        let truckType = self.selectedTruckType?.name ?? ""
        let truckSize = self.selectedTruckSize ?? ""
        let hireFleetData = HireFleetData(weight: weight,
                                          pickUpLocation: pickUpLocation,
                                          pickUpLatitude: pickUpLatitude,
                                          pickUpLongitude: pickUpLongitude,
                                          dropOffLocation: dropOffLocation,
                                          dropOffLatitude: dropOffLatitude,
                                          dropOffLongitude: dropOffLongitude,
                                          pickUpCity: pickUpCity,
                                          dropOffCity: dropOffCity,
                                          pickUpDate: pickUpDate,
                                          vendor: vendor,
                                          commodity:commodity,
                                          truckType:truckType,
                                          truckSize:truckSize)
        return hireFleetData
    }
    private func resetWeightPricing(){
        self.tfWeightInTons.text = nil
    }
    private func validate(sender:BlueGradient){
        if self.pickUpLocation == nil || self.dropOffLocation == nil{
            Utility.main.showToast(message: Strings.SELECT_LOCATION.text)
            sender.shake()
            return
        }
        if self.selectedCommodity == nil{
            Utility.main.showToast(message: Strings.SELECT_COMMODITY.text)
            sender.shake()
            return
        }
        if self.selectedTruckType == nil{
            Utility.main.showToast(message: Strings.SELECT_TRUCK_TYPE.text)
            sender.shake()
            return
        }
//        if self.selectedTruckSize == nil{
//            Utility.main.showToast(message: Strings.SELECT_TRUCK_SIZE.text)
//            sender.shake()
//            return
//        }
        if self.selectedWeight == nil{
            Utility.main.showToast(message: Strings.SELECT_WEIGHT_RANGE.text)
            sender.shake()
            return
        }
        if self.selectedDate == nil{
            Utility.main.showToast(message: Strings.SELECT_DATE.text)
            sender.shake()
            return
        }
        super.pushToFleets(hireFleetData: self.getHireFleetData())
    }
    private func setUI(){
        self.title = Strings.HOME.text
        self.tfDate.tintColor = .white
    }
}
//MARK:- Reverse Geocode Location
extension Home{
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        // Update View
        Utility.hideLoader()
        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")
            switch self.locationPickerType{
            case .pickUp:
                self.lblPickUp.text = "Unable to Find Address for Location"
            case .dropOff:
                self.lblDropOff.text = "Unable to Find Address for Location"
            }
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                switch self.locationPickerType{
                case .pickUp:
                    self.lblPickUpTitle.text = placemark.name ?? "Pick-up Location"
                    self.lblPickUp.text = placemark.compactAddress
                    self.pickUpLocation = placemark.location?.coordinate ?? CLLocationCoordinate2D()
                    self.pickUpCity = placemark.locality
                    self.addPickUpMarker()
                    if let _ = self.dropOffLocation{
                        self.hidePicker()
                        self.fitAllMarkersBounds()
                        self.getRoute()
                    }
                case .dropOff:
                    self.lblDropOffTitle.text = placemark.name ?? "Drop-off Location"
                    self.lblDropOff.text = placemark.compactAddress
                    self.dropOffLocation = placemark.location?.coordinate ?? CLLocationCoordinate2D()
                    self.dropOffCity = placemark.locality
                    self.addDropOffMarker()
                    self.hidePicker()
                    self.getRoute()
                }
            } else {
                switch self.locationPickerType{
                case .pickUp:
                    self.lblPickUp.text = "No Matching Addresses Found"
                case .dropOff:
                    self.lblDropOff.text = "No Matching Addresses Found"
                }
            }
        }
    }
}
//MARK:- GMSMapViewDelegate
extension Home: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if self.imgPickUpMarker.isHidden{
            //self.fitAllMarkersBounds()
            return
        }
        let coordinates = mapView.projection.coordinate(for: mapView.center)
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        if self.flagShouldReverseGeoCode{
            self.reverseGeoCodeBy(location: location)
        }
    }
    func mapView(_ mapView: GMSMapView, didTapMyLocation location: CLLocationCoordinate2D) {
        self.zoomToSearchLocation(location: location)
    }
    private func reverseGeoCodeBy(location:CLLocation){
        Utility.showLoader()
        self.view.endEditing(true)
        self.geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            // Process Response
            self.processResponse(withPlacemarks: placemarks, error: error)
        }
    }
}
//MARK:- GMSMapViewDelegate
extension Home: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfDate{
            self.setDate(date: self.minDate)
            return true
        }
        if self.pickUpLocation == nil || self.dropOffLocation == nil{
            Utility.main.showToast(message: Strings.SELECT_LOCATION.text)
            return false
        }
        switch textField{
        case self.tfCommodity:
            self.commodityDropDown.show()
            return false
        case self.tfTruckType:
            self.truckTypeDropDown.show()
            return false
        case self.tfTruckSize:
            self.truckSizeDropDown.show()
            return false
        case self.tfWeightInTons:
            self.dropDown.show()
            return false
        default:
            return true
        }
    }
}
//MARK:- CLLocationManagerDelegate
extension Home: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            self.mapView.isMyLocationEnabled = true
            self.locationManager.startUpdatingLocation()
            break
        case .denied:
            self.mapView.isMyLocationEnabled = false
            self.currentLocation = nil
            print("You denied the permission, your location tracking won't work")
            break
        default:
            print("Unknown Status")
            break
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentlocation = locations.first else {return}
        self.currentLocation = currentlocation.coordinate
        if self.pickUpLocation == nil{
            self.pickUpLocation = currentlocation.coordinate
            self.zoomToSearchLocation(location: self.pickUpLocation ?? CLLocationCoordinate2D())
            guard let pickUp = self.pickUpLocation else {return}
            let location = CLLocation(latitude: pickUp.latitude, longitude: pickUp.longitude)
            self.reverseGeoCodeBy(location: location)
            self.flagShouldReverseGeoCode = true
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
}
//MARK:- Polyline
extension Home{
    private func getRoute(){
        self.resetWeightPricing()
        let origin = "\(self.pickUpLocation?.latitude ?? 0.0),\(self.pickUpLocation?.longitude ?? 0.0)"
        let destination = "\(self.dropOffLocation?.latitude ?? 0.0),\(self.dropOffLocation?.longitude ?? 0.0)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constants.apiKey)"
        Alamofire.request(url).responseJSON { response in
            do {
                let json = try JSON(data: response.data ?? Data())
                let routes = json["routes"].arrayValue
                self.drawRoute(routesArray: routes)
            } catch {
                print("Hm, something is wrong here. Try connecting to the wifi.")
            }
        }
    }
    private func drawRoute(routesArray: [JSON]) {
        self.resetPolyline()
        if !routesArray.isEmpty{
            let routeDict = routesArray[0]
            let routeOverviewPolyline = routeDict["overview_polyline"].dictionary
            let points = routeOverviewPolyline?["points"]?.stringValue
            self.path = GMSPath.init(fromEncodedPath: points ?? "")!
            self.polyline.path = path
            self.polyline.strokeColor = Global.APP_COLOR
            self.polyline.strokeWidth = 3.0
            self.polyline.map = self.mapView
            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
            self.fitAllMarkersBounds()
        }
    }
    @objc private func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.lightGray
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.resetPolyline()
        }
    }
    private func resetPolyline(){
        self.i = 0
        self.animationPath = GMSMutablePath()
        self.animationPolyline.map = nil
    }
    private func getDistance()->String?{
        if self.path.count() == 0{return nil}
        var distance = 0.0
        let pathTaken = self.path.count()
        let p1 = path.coordinate(at: 0)
        var point1 = CLLocation(latitude: p1.latitude , longitude: p1.longitude)
        if pathTaken > 0{
            for i in 1..<pathTaken{
                let p2 = path.coordinate(at: i)
                let point2 = CLLocation(latitude: p2.latitude , longitude: p2.longitude)
                let dist = Double(point1.distance(from: point2))
                distance = distance + dist
                point1 = point2
            }
        }
        let distanceInKM = "\(distance/1000.0)"
        print(distanceInKM)
        return distanceInKM
    }
}
//MARK:- Add marker
extension Home{
    private func addPickUpMarker(){
        //DispatchQueue.main.async {
            var markerIcon: UIImageView?
            let pickUpPin = UIImage(named: "location_p")!.withRenderingMode(.alwaysOriginal)
            let markerView = UIImageView(image: pickUpPin)
            markerIcon = markerView
            markerIcon?.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
            self.pickUpLocationMarker.map = nil
            self.pickUpLocationMarker = GMSMarker(position: self.pickUpLocation ?? CLLocationCoordinate2D())
            self.pickUpLocationMarker.iconView = markerIcon
            self.pickUpLocationMarker.tracksViewChanges = true
            self.pickUpLocationMarker.map = self.mapView
        //}
    }
    private func addDropOffMarker(){
        //DispatchQueue.main.async {
            var markerIcon: UIImageView?
            let dropOffPin = UIImage(named: "location_d")!.withRenderingMode(.alwaysOriginal)
            let markerView = UIImageView(image: dropOffPin)
            markerIcon = markerView
            markerIcon?.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
            self.dropOffLocationMarker.map = nil
            self.dropOffLocationMarker = GMSMarker(position: self.dropOffLocation ?? CLLocationCoordinate2D())
            self.dropOffLocationMarker.iconView = markerIcon
            self.dropOffLocationMarker.tracksViewChanges = true
            self.dropOffLocationMarker.map = self.mapView
            self.fitAllMarkersBounds()
        //}
    }
}
//MARK:- Date Picker
extension Home{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .dateAndTime
        datePickerView.minimumDate = self.minDate
        datePickerView.maximumDate = self.maxDate
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd, MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
        self.selectedDate = date
    }
}
extension Home: GMSAutocompleteViewControllerDelegate{
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //        print("Place name: \(place.name)")
        //        print("Place ID: \(place.placeID)")
        //        print("Place attributions: \(place.attributions)")
        self.dismiss(animated: true, completion: {
        self.setPickedAddressFromPlacePickerVC(place)
        })
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        Utility.main.showToast(message: "Error: "+error.localizedDescription)
        print("Error: ", error.localizedDescription)
    }
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
//MARK:- Services
extension Home{
    private func getAllAttributes(){
        APIManager.sharedInstance.usersAPIManager.GetAttributes(params: [:], success: { (responseObject) in
            guard let attribute = Mapper<AttributesModel>().map(JSON: responseObject) else{return}
            self.arrCommodity = attribute.comidity
            self.arrTruckType = attribute.vehileTypes
            self.arrWeights = attribute.weight
            self.setDropDowns()
        }) { (error) in
            print(error)
        }
    }
}
