//
//  Fleets.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet

class Fleets: BaseController {

    @IBOutlet weak var tfSearchField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var hireFleetData:HireFleetData?
    var arrFleets = [FleetsModel]()
    var arrFleetsRiderType = [FleetsModel]()
    var arrFilteredFleetsRiderType = [FleetsModel]()
    var riderType = RiderType.both
    var isFilter = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.FLEETS.text
        self.tableView.emptyDataSetSource = self
        self.getFleets()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onTfSearchField(_ sender: UITextField) {
        let search = sender.text ?? ""
        if search.isEmpty{
            self.isFilter = false
        }
        else{
            self.isFilter = true
            self.arrFilteredFleetsRiderType = self.arrFleetsRiderType.filter { ($0.firstName ?? "").localizedCaseInsensitiveContains(search) }
        }
        self.tableView.reloadData()
    }
    @IBAction func onBtnFilter(_ sender: Any) {
        self.presentFleetFilter()
    }
    @IBAction func onBtnTrustTransmissito(_ sender: UIButton){
        Utility.main.showAlert(message: Strings.BROADCAST_THIS_RIDE.text, title: Strings.TRUST_TRANSMISSITO.text, controller: self) { (yes, no) in
            if yes != nil{
                self.processBroadcastRide()
            }
        }
    }
}
//MARK:- Helper Methods
extension Fleets{
    private func presentFleetFilter(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "FleetsFilter") as! FleetsFilter
        controller.selectedRide = { riderType in
            self.riderType = riderType
//            self.categoryFilter()
        }
        controller.riderType = self.riderType
        self.present(controller, animated: true, completion: nil)
    }
//    private func categoryFilter(){
//        self.tfSearchField.text = ""
//        self.tfSearchField.resignFirstResponder()
//        self.isFilter = false
//        switch self.riderType{
//        case .both:
//            self.arrFleetsRiderType = self.arrFleets
//        case .individual,.company:
//            self.arrFleetsRiderType = self.arrFleets.filter{"\($0.riderType)" == self.riderType.rawValue}
//        }
//        self.tableView.reloadData()
//    }
}
//MARK:- UITableViewDataSource
extension Fleets: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.isFilter{
        case true:
            return self.arrFilteredFleetsRiderType.count
        case false:
            return self.arrFleetsRiderType.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.isFilter{
        case true:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FleetsTVC", for: indexPath) as! FleetsTVC
            cell.setData(data: self.arrFilteredFleetsRiderType[indexPath.row])
            return cell
        case false:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FleetsTVC", for: indexPath) as! FleetsTVC
            cell.setData(data: self.arrFleetsRiderType[indexPath.row])
            return cell
        }
    }
}
//MARK:- UITableViewDelegate
extension Fleets: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var fleet = FleetsModel()
        switch self.isFilter{
        case true:
            fleet = self.arrFilteredFleetsRiderType[indexPath.row]
        case false:
            fleet = self.arrFleetsRiderType[indexPath.row]
        }
        super.pushToFleetDetails(fleet: fleet, hireFleetData: self.hireFleetData)
    }
}
//MARK:- DZNEmptyDataSetSource
extension Fleets:DZNEmptyDataSetSource{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_RESULTS.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = ""
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}
//MARK:- UITableViewDelegate
extension Fleets{
    private func getFleets(){
        let pickUpCity = self.hireFleetData?.pickUpCity ?? ""
        let dropOffCity = self.hireFleetData?.dropOffCity ?? ""
        let weightRange = self.hireFleetData?.weight ?? ""
        let truckType = self.hireFleetData?.truckType ?? ""
    
        let params:[String:Any] = ["pickUpCity":pickUpCity,"dropOffCity":dropOffCity,"weightRange":weightRange,"truckType":truckType]
        print(params)
        APIManager.sharedInstance.usersAPIManager.FleetList(params: params, success: { (responseObject) in
//            (responseObject[0] as! Dictionary<String, Any>).printJson()
            guard let fleets = responseObject as? [[String:Any]] else {return}
            self.arrFleets = Mapper<FleetsModel>().mapArray(JSONArray: fleets)
            print(self.arrFleets)
            self.arrFleetsRiderType = self.arrFleets
            self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
    }
    private func processBroadcastRide(){
        Utility.main.showAlert(message: Constants.ApiMessage, title: Strings.Confirmation.text, controller: self) {
            AppDelegate.shared.changeRootViewController()
        }
//        guard let data = self.hireFleetData else {return}
//        let weight_in_tons     = data.weight_in_tons
//        let pickup_location    = data.pickup_location
//        let pickup_latitude    = data.pickup_latitude
//        let pickup_longitude   = data.pickup_longitude
//        let drop_off_location  = data.drop_off_location
//        let drop_off_latitude  = data.drop_off_latitude
//        let drop_off_longitude = data.drop_off_longitude
//        let amount             = data.amount
//        let date_time          = data.date_time
//        let fleet_id           = 0
//        let params:[String:Any] = ["weight_in_tons":weight_in_tons,
//                                   "pickup_location":pickup_location,
//                                   "pickup_latitude":pickup_latitude,
//                                   "pickup_longitude":pickup_longitude,
//                                   "drop_off_location":drop_off_location,
//                                   "drop_off_latitude":drop_off_latitude,
//                                   "drop_off_longitude":drop_off_longitude,
//                                   "amount":amount,
//                                   "date_time":date_time,
//                                   "fleet_id":fleet_id]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.HireFleet(params: params, success: { (responseObject) in
//            Utility.main.showAlert(message: Constants.ApiMessage, title: Strings.Confirmation.text, controller: self) {
//                AppDelegate.shared.changeRootViewController()
//            }
//        }) { (error) in
//            print(error)
//        }
    }
}
