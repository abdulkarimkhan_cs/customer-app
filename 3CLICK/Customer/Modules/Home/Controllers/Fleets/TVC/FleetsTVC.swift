//
//  FleetsTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import SDWebImage

class FleetsTVC: UITableViewCell {

    @IBOutlet weak var imgFleet: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    
    func setData(data:FleetsModel){
        let imgURLString = data.profilePic ?? ""
        if let imgURL = URL(string: imgURLString){
            self.imgFleet.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "icon"), options: .highPriority, completed: nil)
        }
        self.lblName.text = data.companyName ?? "-"
        let amount = "\(Strings.CURRENCY.text) \(data.amount ?? "0")"
        self.lblRate.text = amount
        let rating = data.rating
        self.viewRating.rating = rating
    }
}
