//
//  FleetDetails.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class FleetDetails: BaseController {
    
    @IBOutlet weak var imgFleetBanner: UIImageView!
    @IBOutlet weak var imgFleet: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewDescription: CardView!
    
    var fleet:FleetsModel?
    var hireFleetData:HireFleetData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.FLEET_DETAILS.text
        self.setData(data: self.fleet ?? FleetsModel())
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnHire(_ sender: BlueGradient) {
        let title = Strings.HIRE_FLEET.text
        let message = Strings.SELECT_PAYMENT_METHOD.text
        Utility.main.showAlert(message: message, title: title, YES: Strings.PAY_NOW.text, NO: Strings.PAY_BY_WALLET.text, controller: self) { (now, wallet) in
            if now != nil{
                self.processHireFleet()
            }
            if wallet != nil{
                
            }
        }
    }
}
//MARK:- Services
extension FleetDetails{
    private func setData(data:FleetsModel){
        let imgbannerURLString = data.profilePic ?? ""
        if let imgURL = URL(string: imgbannerURLString){
            self.imgFleetBanner.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "trucks"), options: .highPriority, completed: nil)
        }
        let imgURLString = data.profilePic ?? ""
        if let imgURL = URL(string: imgURLString){
            self.imgFleet.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "icon"), options: .highPriority, completed: nil)
        }
        self.lblName.text = data.companyName ?? "-"
        self.lblRate.text = "\(Strings.CURRENCY.text) \(data.amount ?? "")"
        let rating = Double(data.rating)
        self.viewRating.rating = rating
        self.lblDescription.text = data.aboutCompany ?? ""
    }
}
//MARK:- Services
extension FleetDetails{
    private func processHireFleet(){
        guard let hireFleetData = self.hireFleetData else {return}
        let weight = hireFleetData.weight
        let pickUpLocation = hireFleetData.pickUpLocation
        let pickUpLatitude = hireFleetData.pickUpLatitude
        let pickUpLongitude = hireFleetData.pickUpLongitude
        let dropOffLocation = hireFleetData.dropOffLocation
        let dropOffLatitude = hireFleetData.dropOffLatitude
        let dropOffLongitude = hireFleetData.dropOffLongitude
//        let pickUpCity = hireFleetData.pickUpCity
//        let dropOffCity = hireFleetData.dropOffCity
        let pickUpDate = hireFleetData.pickUpDate
        let commodity = hireFleetData.commodity
        let truckType = hireFleetData.truckType
        let vendor = self.fleet?.id ?? "0"
        let rateId = self.fleet?.rateId ?? "0"
        let truckSize = hireFleetData.truckSize
        
        let params:[String:Any] = ["weight":weight,
                                   "pickUpLocation":pickUpLocation,
                                   "pickUpLatitude":pickUpLatitude,
                                   "pickUpLongitude":pickUpLongitude,
                                   "dropOffLocation":dropOffLocation,
                                   "dropOffLatitude":dropOffLatitude,
                                   "dropOffLongitude":dropOffLongitude,
//                                   "pickUpCity":pickUpCity,
//                                   "dropOffCity":dropOffCity,
                                   "pickUpDate":pickUpDate,
                                   "commodity":commodity,
                                   "truckType":truckType,
                                   "vendor":vendor,
                                   "rateId":rateId,
                                   "truckSize":truckSize]
        print(params)
        APIManager.sharedInstance.usersAPIManager.HireFleet(params: params, success: { (responseObject) in
            Utility.main.showAlert(message: Constants.ApiMessage, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler: {
                AppDelegate.shared.changeRootViewController()
            })
        }) { (error) in
            print(error)
        }
    }
}
