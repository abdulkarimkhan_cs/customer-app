//
//  CMS.swift
//  3CLICK
//
//  Created by Shakeel Khan on 10/25/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import WebKit
import ObjectMapper

class CMS: BaseController {
    
    @IBOutlet weak var webView: WKWebView!
    var cms:CMSModel?
    var cmsType = CMSType.help
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.webView.navigationDelegate = self
        self.getContentIfRequiredOrLoadLink()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension CMS{
    private func setUI(){
        switch self.cmsType{
        case .help:
            self.title = Strings.HELP.text
        case .aboutUs:
            self.title = Strings.ABOUT_US.text
        case .termsAndConditions:
            break
        }
    }
    private func getContentIfRequiredOrLoadLink(){
        var URLString = ""
        switch self.cmsType{
        case .help:
            URLString = AppDelegate.shared.cms?.helpFaqUrl ?? ""
        case .aboutUs:
            URLString = AppDelegate.shared.cms?.aboutUs ?? ""
        case .termsAndConditions:
            URLString = AppDelegate.shared.cms?.termsAndConditions ?? ""
        }
        if !URLString.isEmpty{
            if let URL = URL(string: URLString){
                self.webView.load(URLRequest(url: URL))
            }
            else{
                Utility.main.showAlert(message: Strings.URL_NOT_VALID.text, title: Strings.ERROR.text)
            }
        }
        else{
            self.getCMS()
        }
    }
}
//MARK:- WKNavigationDelegate
extension CMS: WKNavigationDelegate{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Utility.showLoader()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Utility.hideLoader()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Utility.hideLoader()
    }
}
//MARK:- Service
extension CMS{
    private func getCMS(){
//        APIManager.sharedInstance.usersAPIManager.CMS(params: [:], success: { (responseObject) in
//            guard let cms = Mapper<CMSModel>().map(JSON: responseObject) else{return}
//            AppDelegate.shared.cms = cms
//            self.getContentIfRequiredOrLoadLink()
//        }) { (error) in
//            print(error)
//        }
    }
}
