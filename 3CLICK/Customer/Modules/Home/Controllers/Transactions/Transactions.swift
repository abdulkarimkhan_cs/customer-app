//
//  Transactions.swift
//  3CLICK
//
//  Created by Mac Book on 11/01/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class Transactions: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrTransactions = [TransactionModel]()
    var transactionsPaged = PagedModel()
    var pageNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.TRANSACTIONS.text
        self.registerCell()
        self.getAllTransactions()
        // Do any additional setup after loading the view.
    }

}
extension Transactions{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "TransactionsTVC", bundle: nil), forCellReuseIdentifier: "TransactionsTVC")
    }
    private func loadMoreCells(){
        let totaTransactions = self.transactionsPaged.count
        if totaTransactions > self.arrTransactions.count && self.pageNumber <= self.transactionsPaged.totalPages{
            self.pageNumber += 1
            self.getAllTransactions()
        }
    }
}
extension Transactions: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTransactions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsTVC", for: indexPath) as! TransactionsTVC
        let data = self.arrTransactions[indexPath.row]
        cell.setData(data: data)
        return cell
    }
}
extension Transactions: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedTransaction = self.arrTransactions[indexPath.row]
        self.presentTransactinsDetail(selectedTransaction: selectedTransaction)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if tableView.visibleCells.contains(cell) {
                if indexPath.row == self.arrTransactions.count - 1{
                    self.loadMoreCells()
                }
            }
        }
    }
}
//MARK:- Services
extension Transactions{
    private func getAllTransactions(){
        let page = self.pageNumber
        let limit = Constants.PAGINATION_PAGE_SIZE
        
        let params:[String:Any] = ["page":page,
                                   "limit":limit]
        
        APIManager.sharedInstance.usersAPIManager.GetAllTransactions(params: params, success: { (responseObject) in
            print(responseObject)
            
            let response = responseObject as Dictionary
            response.printJson()
            guard let transactions = response["transactions"] as? [[String:Any]] else {return}
            
            if self.arrTransactions.isEmpty{
                self.transactionsPaged = Mapper<PagedModel>().map(JSON: responseObject) ?? PagedModel()
                self.arrTransactions = Mapper<TransactionModel>().mapArray(JSONArray: transactions)
            }
            else{
                let transactions = Mapper<TransactionModel>().mapArray(JSONArray: transactions)
                self.arrTransactions += transactions
            }
            self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
    }
}
