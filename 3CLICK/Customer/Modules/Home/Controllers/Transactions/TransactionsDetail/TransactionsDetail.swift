//
//  TransactionsDetail.swift
//  3CLICK
//
//  Created by Mac Book on 11/01/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit

class TransactionsDetail: BaseController {

    @IBOutlet weak var lblBookingBy: UILableDynamicFonts!
    @IBOutlet weak var lblPreviousRs: UILabel!
    @IBOutlet weak var lblDeducted: UILabel!
    @IBOutlet weak var lblUpdated: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var selectedTransaction = TransactionModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setData(){
        print(selectedTransaction)
        self.lblBookingBy.text = self.selectedTransaction.activityType ?? ""
        let date = Utility.stringDateFormatter(dateStr: self.selectedTransaction.createdAt ?? "", dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", formatteddate: "d MMM yy, E h:mm a")

        self.lblDate.text = date
        self.lblPreviousRs.text = self.formatPoints(num: Double(self.selectedTransaction.updatedCredit))
        self.lblDeducted.text = self.formatPoints(num: Double(self.selectedTransaction.amount))
        let updatedAmount = Double(self.selectedTransaction.updatedCredit) - Double(self.selectedTransaction.amount)
        self.lblUpdated.text = self.formatPoints(num: updatedAmount)
    }
    
    func formatPoints(num: Double) ->String{
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(floor(thousandNum) == thousandNum){
                return("\(Int(thousandNum))k")
            }
            return("\(Int(thousandNum))k")
        }
        if num > 1000000{
            if(floor(millionNum) == millionNum){
                return("\(Int(millionNum))M")
            }
            return("\((millionNum).roundToPlaces(places: 1))M")
        }
        else{
            if(floor(num) == num){
                return ("\(Int(num))")
            }
            return ("\(Int(num))")
        }

    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        self.dismiss(animated: true, completion: nil)
    }
}
