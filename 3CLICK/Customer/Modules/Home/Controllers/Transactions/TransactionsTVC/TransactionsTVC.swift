//
//  TransactionsTVC.swift
//  3CLICK
//
//  Created by Mac Book on 11/01/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import UIKit

class TransactionsTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILableDynamicFonts!
    @IBOutlet weak var lblDate: UILableDynamicFonts!
    @IBOutlet weak var lblAmount: UILableDynamicFonts!
    @IBOutlet weak var imgTrasactionStatus: UIImageView!
    
    func setData(data:TransactionModel){
        self.lblTitle.text = data.activityType ?? ""
        self.lblAmount.text = self.formatPoints(num: Double(data.amount))
        let date = Utility.stringDateFormatter(dateStr: data.createdAt ?? "", dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", formatteddate: "d MMM yy, E h:mm a")
        self.lblDate.text = date
        
        if data.transactionType == "deducted" {
            self.imgTrasactionStatus.image = UIImage(named: "downArrowOrange")
        }else{
            self.imgTrasactionStatus.image = UIImage(named: "upArrowOrange")
        }
        
    }
    
    func formatPoints(num: Double) ->String{
           let thousandNum = num/1000
           let millionNum = num/1000000
           if num >= 1000 && num < 1000000{
               if(floor(thousandNum) == thousandNum){
                   return("\(Int(thousandNum))k")
               }
               return("\(Int(thousandNum))k")
           }
           if num > 1000000{
               if(floor(millionNum) == millionNum){
                   return("\(Int(millionNum))M")
               }
               return("\(Int(millionNum))M")
           }
           else{
               if(floor(num) == num){
                   return ("\(Int(num))")
               }
               return ("\(Int(num))")
           }

       }
}

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

