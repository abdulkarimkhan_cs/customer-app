//
//  Login.swift
//  3CLICK
//
//  Created by Sierra-PC on 17/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class Login: BaseController {
    
    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfEmailAddress.text = "test123@mailinator.com"
        self.tfPassword.text     = "123456"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.tfPassword.isSecureTextEntry = !self.tfPassword.isSecureTextEntry
    }
    @IBAction func onBtnForgotPassword(_ sender: UIButton) {
        super.pushToForgotPassword()
    }
    @IBAction func onBtnLogin(_ sender: BlueGradient) {
       self.validate(sender: sender)
    }
    @IBAction func onBtnCreateAccount(_ sender: UIButton) {
        super.pushToSignUp()
    }
    
}
//MARK:- Helper Methods
extension Login{
    private func validate(sender:UIButton){
        do {
            let _ = try self.tfEmailAddress.validatedText(validationType: ValidatorType.email)
            let _ = try self.tfPassword.validatedText(validationType: ValidatorType.password)
            self.login()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
}
//MARK:- Services
extension Login{
    private func login(){
        let email = self.tfEmailAddress.text ?? ""
        let password = self.tfPassword.text ?? ""
        let deviceId = Constants.DeviceToken
        let params:[String:Any] = ["email":email,
                                   "password":password,
                                   "deviceId":deviceId]
    
        APIManager.sharedInstance.usersAPIManager.LoginUser(params: params, success: { (responseObject) in
            let response = responseObject as Dictionary
            response.printJson()
            guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
            if user.status != "verified"{
                Constants.Token = user.accessToken ?? ""
                super.pushToCodeVerification(user: user)
            }
            else{
                AppStateManager.sharedInstance.loginUser(user: user)
            }
        }) { (error) in
            print(error)
        }
    }
}
