//
//  SignUp.swift
//  3CLICK
//
//  Created by Sierra-PC on 24/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import PhoneNumberKit
import ObjectMapper
import DropDown

class SignUp: BaseController {

    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tflastName: UITextField!
    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var tfCountryCode: UITextFieldDynamicFonts!
    @IBOutlet weak var tfPhoneNumber: UITextFieldDynamicFonts!
    @IBOutlet weak var tfState: UITextFieldDynamicFonts!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    let stateDropDown = DropDown()
    var arrStates = [CityModel]()
    var selectedState: CityModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.getStates()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnCountryCode(_ sender: UIButton) {
        self.openCountryPicker()
    }
    @IBAction func onBtnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        case 0:
            self.tfPassword.isSecureTextEntry = !self.tfPassword.isSecureTextEntry
        case 1:
            self.tfConfirmPassword.isSecureTextEntry = !self.tfConfirmPassword.isSecureTextEntry
        default:
            break
        }
    }
    @IBAction func onBtnTermsCheckBox(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func onBtnTermsAndConditions(_ sender: UIButton) {
        super.pushToTermsAndConditions()
    }
    @IBAction func onBtnSignUp(_ sender: UIButton) {
        self.validate(sender: sender)
    }

}
//MARK:- Helper Methods
extension SignUp{
    private func validate(sender:UIButton){
        let number = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        do {
            let _ = try self.tfUserName.validatedText(validationType: ValidatorType.username)
            let _ = try self.tfEmailAddress.validatedText(validationType: ValidatorType.email)
            if !Utility.validatePhoneNumber(number: number){
                sender.shake()
                Utility.main.showToast(message: Strings.INVALID_PHONE.text)
                return
            }
            let _ = try self.tfState.validatedText(validationType: ValidatorType.state)
            let _ = try self.tfPassword.validatedText(validationType: ValidatorType.password)
            if (self.tfPassword.text ?? "") != (self.tfConfirmPassword.text ?? ""){
                sender.shake()
                Utility.main.showToast(message: Strings.PWD_DONT_MATCH.text)
                return
            }
            if !self.btnCheckBox.isSelected{
                sender.shake()
                Utility.main.showToast(message: Strings.PLEASE_AGREE_TO_TNC.text)
                return
            }
            self.processSignUp()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
    private func openCountryPicker(){
        let controller = MICountryPicker()
        controller.delegate = self
        controller.showCallingCodes = true
        //controller.setStatusBarStyle(.lightContent)
        self.present(controller, animated: true, completion: nil)
    }
    private func setStatesDropDown(){
        self.stateDropDown.dataSource = self.arrStates.map{$0.name ?? "-"}
        self.stateDropDown.direction = .any
        self.stateDropDown.anchorView = self.tfState
        self.stateDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfState.text = item
            self.selectedState = self.arrStates[index]
        }
    }
    private func setUI(){
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: "PK")?.image()
        self.tfCountryCode.text = "+92"
        self.setStatesDropDown()
    }
}
//MARK:- UITextFieldDelegate
extension SignUp: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfState{
            self.stateDropDown.show()
            return false
        }
        return true
    }
}
//MARK:- MICountryPickerDelegate
extension SignUp: MICountryPickerDelegate{
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        self.dismiss(animated: true, completion: nil)
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: code)?.image()
    }
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.tfCountryCode.text = dialCode
        self.tfPhoneNumber.becomeFirstResponder()
    }
}
//MARK:- Services
extension SignUp{
    private func getStates() {
        print("Check")
        APIManager.sharedInstance.usersAPIManager.GetStates(params: [:], success: { (responseObject) in
            guard let response = responseObject as? [[String:Any]] else {return}
            print(response)
            self.arrStates = Mapper<CityModel>().mapArray(JSONArray: response)
            self.setStatesDropDown()
        }) { (error) in
            print(error)
        }
    }
    private func processSignUp() {
        
        let firstName = self.tfUserName.text ?? ""
        let lastName = self.tflastName.text ?? ""
        let email = self.tfEmailAddress.text ?? ""
        let contact = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        let city = self.selectedState?.id ?? "0"
        let password = self.tfPassword.text ?? ""

        let params:[String:Any] = ["firstName":firstName,
                                   "email":email,
                                   "contact":contact,
                                   "city":city,
                                   "password":password,
                                   "lastName":lastName]
        print(params)
        
        APIManager.sharedInstance.usersAPIManager.SignUpUser(params: params, success: { (responseObject) in
            print(responseObject)
            guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
            Constants.Token = user.accessToken ?? ""
            super.pushToCodeVerification(user: user)
        }) { (error) in
            print(error)
        }
    }
}
