//
//  WeightsModel.swift
//  3CLICK
//
//  Created by Sierra-PC on 24/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation

import ObjectMapper
import RealmSwift

public class WeightsModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let weightInTonsValue = "weight_in_tons_value"
        static let weightInTonsId = "weight_in_tons_id"
    }
    // MARK: Properties
    @objc dynamic var weightInTonsValue: String? = "8"
    @objc dynamic var weightInTonsId: String? = ""
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "weightInTonsId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        weightInTonsValue <- map[SerializationKeys.weightInTonsValue]
        weightInTonsId <- map[SerializationKeys.weightInTonsId]
    }
}
