//
//  NotificationModel.swift
//  3CLICK
//
//  Created by Sierra-PC on 04/07/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

public class TransactionModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let activityType = "activityType"
        static let updatedCredit = "updatedCredit"
        static let transactionType = "transactionType"
        static let amount = "amount"
        static let dateTime = "dateTime"
        static let createdAt = "createdAt"
        static let id = "_id"
    }
    // MARK: Properties
    @objc dynamic var activityType: String? = ""
    @objc dynamic var updatedCredit = 0
    @objc dynamic var transactionType: String? = ""
    @objc dynamic var amount = 0
    @objc dynamic var dateTime = 0
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var id: String? = ""
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        activityType <- map[SerializationKeys.activityType]
        updatedCredit <- map[SerializationKeys.updatedCredit]
        transactionType <- map[SerializationKeys.transactionType]
        amount <- map[SerializationKeys.amount]
        dateTime <- map[SerializationKeys.dateTime]
        createdAt <- map[SerializationKeys.createdAt]
        id <- map[SerializationKeys.id]
    }
}
