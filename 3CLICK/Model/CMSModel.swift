import Foundation

import ObjectMapper
import RealmSwift

public class CMSModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let helpFaqUrl = "help_faq_url"
        static let termsAndConditions = "terms_and_conditions"
        static let privacyPolicy = "privacy_policy"
        static let aboutUs = "about_us"
        static let cmsId = "cms_id"
    }
    // MARK: Properties
    @objc dynamic var helpFaqUrl: String? = ""
    @objc dynamic var termsAndConditions: String? = ""
    @objc dynamic var privacyPolicy: String? = ""
    @objc dynamic var aboutUs: String? = ""
    @objc dynamic var cmsId = 0
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "cmsId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        helpFaqUrl <- map[SerializationKeys.helpFaqUrl]
        termsAndConditions <- map[SerializationKeys.termsAndConditions]
        privacyPolicy <- map[SerializationKeys.privacyPolicy]
        aboutUs <- map[SerializationKeys.aboutUs]
        cmsId <- map[SerializationKeys.cmsId]
    }
}
