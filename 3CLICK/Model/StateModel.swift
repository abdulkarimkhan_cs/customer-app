//
//  WeightsModel.swift
//  3CLICK
//
//  Created by Sierra-PC on 24/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation

import ObjectMapper
import RealmSwift

public class StateModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let stateName = "state_name"
        static let stateId = "state_id"
    }
    // MARK: Properties
    @objc dynamic var stateName: String? = ""
    @objc dynamic var stateId: String? = ""
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "stateId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        stateName <- map[SerializationKeys.stateName]
        stateId <- map[SerializationKeys.stateId]
    }
}
