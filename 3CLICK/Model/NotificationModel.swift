//
//  NotificationModel.swift
//  3CLICK
//
//  Created by Sierra-PC on 04/07/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

public class NotificationModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let title = "title"
        static let message = "message"
        static let id = "_id"
    }
    // MARK: Properties
    @objc dynamic var title: String? = ""
    @objc dynamic var message: String? = ""
    @objc dynamic var id: String? = ""
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "title"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        title <- map[SerializationKeys.title]
        message <- map[SerializationKeys.message]
        id <- map[SerializationKeys.id]
    }
}
