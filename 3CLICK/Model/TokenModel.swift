
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class TokenModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let token = "token"
        static let user = "User"
    }
    // MARK: Properties
    @objc dynamic var token: String? = ""
    @objc dynamic var user: UserModel?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "token"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        token <- map[SerializationKeys.token]
        user <- map[SerializationKeys.user]
    }
}
