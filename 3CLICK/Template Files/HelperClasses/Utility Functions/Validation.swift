
import Foundation
import UIKit
class Validation {
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isValidPassword(_ value: String) -> Bool {
        var result = false
        if value.count >= 6 {
            result = true
        }
        return result
    }
    
    static func isValidName(_ value: String) -> Bool {
        if value.count > 2{
            return true
        }
        return false
    }
    
    static func isValidPhoneNumber(_ value: String) -> Bool {
        guard let _ = Int(value),value.count > 7 else{return false}
        return true
    }
    
    static func isConfirmPasswordIsEqualToPassword(password: String, confirm: String) -> Bool {
        if(password == confirm){
            return true
        }
        return false
    }
    
    static func validateStringLength(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return !trimmed.isEmpty
    }
    
    static func validateDouble(_ text: String) -> Bool {
        if let _ = Double(text){
            return true
        }
        else{
            return false
        }
    }
}
