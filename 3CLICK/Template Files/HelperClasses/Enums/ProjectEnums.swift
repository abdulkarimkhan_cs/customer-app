//
//  ProjectEmuns.swift
//  3CLICK
//
//  Created by Sierra-PC on 01/07/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation

enum RiderType:String{
    case both = "0"
    case individual = "4"
    case company = "3"
}

enum BookingType:String{
    case current = "current"
    case pending  = "pending"
    case finished = "finished"
    case inProgress = "inProgress"
    case cancelled = "cancelled"
}

enum CMSType{
    case help
    case aboutUs
    case termsAndConditions
}
