
import Foundation
import UIKit
import GoogleMaps

struct Global{
    static let LOGGED_IN_USER                = AppStateManager.sharedInstance.loggedInUser
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
    static var APP_COLOR                     = UIColor(red: 249/255, green: 104/255, blue: 22/255, alpha: 1.0)
    static var APP_COLOR_LIGHT               = UIColor(red: 44/255, green: 167/255, blue: 207/255, alpha: 1.0)
    static var APP_COLOR_GREY                = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
}

struct Constants {
    //MARK:- Base URL
    //static let BaseURL                     = "http://portal.transmissito.com/api/v1/"
    static let BaseURL                     = "http://truck-online.herokuapp.com/api/v1/"
    //static let BaseURL                     = "http://192.168.10.73:8000/api/v1/"
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    static let USER_DEFAULTS               = UserDefaults.standard
    static let DEFAULTS_USER_KEY           = "User"
    static var DeviceToken                 = "No certificates"
    static var ApiMessage                  = "Booking recieved we'll get in touch with you soon"
    static let serverDateFormat            = "yyyy-MM-dd'T'HH:mm:ss"
    static let PAGINATION_PAGE_SIZE        = 10
    static var Token                       = ""
    static let adminPhone                  = "123456"
    //MARK:- Notification observer names
    static let NotificationCount           = "NotificationCount"
    static let apiKey                      = "AIzaSyDfI1D2jIApe9Rknmby_t8neXIT4-yZieo"
    static let publishableKey              = "pk_test_Q9Q8LheTe0VFThTORIy8uRev"
    static let secretKey                   = "sk_test_yEKtZ2nh7WL35f11GcyfAX6a"
    static var driverLocation              : CLLocationCoordinate2D?
}
