//
//  Structs.swift
//  3CLICK
//
//  Created by Mac Book on 23/01/2020.
//  Copyright © 2020 Sierra-PC. All rights reserved.
//

import Foundation

struct NotificationPayload {
    var action: String
    var id: String
    var bookingId: String
}
