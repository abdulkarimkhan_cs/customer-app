
import Foundation
import UIKit
import Alamofire
import SwiftyJSON

enum Route: String {
    //MARK:- USER
    case GetStates = "cities/all"
    case Signup = "users/register"
    case verifyOtp = "users/verify"
    case Login = "users/login"
    case updateProfile = "users/profile/me"
    case cancelBooking = "bookings/status/"
    case getNotifications = "me/notifications"
    case RemoveAllNotifications = "me/notification/read"
    case getTransactions = "users/get/transactions"
    case addRating = "ratings/add"
    
    //MARK:- Home
    case Attributes = "vehicle/attributes"
    
    //MARK:- Vendor
    case SearchVendors = "searchvendors"
    case Bookings = "me/bookings"
    case RideDetail = "bookings/"
    case upload = "images/upload"
    
    case ForgotPassword = "users/forgetPassword"
    case changePassword = "users/changePassword"
    case VerifyEmailCode = "verifyEmail"
    case resendOtp = "resend_email_code"
    case resentCode = "users/resendotp"
    case resetPassword = "users/resetPassword"
    case commonPages = "common_pages"
    
    case WeightInTons = "get_weights"
    case AmountByWeight = "get_amount_by_weight"
    case FleetList = "get_fleets"
    
    case getCards = "get_all_cards"
    case addCardDetails = "add_card_details"
    case updateCardStatus = "update_card_status"
    case deleteCardDetails = "delete_card_details"
    
    
    
    
    case Notification = "get_notification"
    
    case RideList = "get_ride_list"
    
    case Help = "get_help"
    case About = "get_about"
    
    case FleetDetail = "get_fleet_detail"
    case HireFleet = "bookings/add"
    
    
    case reportAProblem = "report_a_problem"
    case getRiderDeviceToken = "get_rider_device_token"
    case updateCustomerId = "update_customer_id"
    case addCard = "get_customer_id"
    
    
}
class APIManagerBase: NSObject {
    let baseURL = Constants.BaseURL
    let defaultRequestHeader = ["Content-Type": "application/json"]
    let defaultError = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Request Failed."])
    
    func getAuthorizationHeader () -> Dictionary<String,String> {
        if let token = APIManager.sharedInstance.serverToken {
            return ["Authorization":"\(token)"]
        }
        return ["Content-Type":"application/json"]
    }
    
    func URLforRoute(route: String,params:[String: Any]) -> NSURL? {
        if let components: NSURLComponents  = NSURLComponents(string: (Constants.BaseURL+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params {
                queryItems.append(NSURLQueryItem(name:key,value: "\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            return components.url as NSURL?
        }
        return nil
    }
    
    func POSTURLforRoute(route:String) -> URL?{
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.BaseURL+route)){
            return components.url! as URL
        }
        return nil
    }
    
}
//MARK:- Response Handle
extension APIManagerBase{
    fileprivate func responseResult(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void) {
        
        Utility.hideLoader()
        let errorGenericMessage = Strings.ERROR_GENERIC_MESSAGE.text
        print(response)
        switch response.result{
        case .success:
            if let dictData = response.result.value as? NSDictionary {
                let response = dictData as Dictionary
                print("--------")
                response.printJson()
                print("--------")
                if let status = dictData["success"] as? Bool,status{
                    success(dictData["data"] as AnyObject)
                    return
                }
                else{
                    if let message = dictData["message"] as? String{
                        if let code = dictData["code"] as? Int, code == 401{
                            Utility.main.showAlert(message: message, title: Strings.ERROR.text, controller: Utility.main.topViewController() ?? UIViewController()) {
                                AppStateManager.sharedInstance.logoutUserNow()
                            }
                            return
                        }
                        Utility.main.showAlert(message: message, title: Strings.ERROR.text)
                    }
                    else{
                        //Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
                        let errorMessage: String = Strings.ERROR.text;
                        let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                        let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                        failure(error)
                    }
                }
            } else {
                //Failure
                let errorMessage: String = Strings.UNKNOWN_ERROR.text;
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                failure(error)
                Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
            }
        case .failure(let error):
            failure(error as NSError)
            Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
        }
    }
}

//MARK:- Get APIs
extension APIManagerBase{
    
    func getArrayResponseWith(route: URL,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure,
                              withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {
                    response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })}
        }
        else{
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding()).responseJSON {
                response in
                
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getDictionaryResponseWith(route: URL,
                                   success:@escaping DefaultAPISuccessClosure,
                                   failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding()).responseJSON {
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getBoolResponseWith(route: URL,
                             success:@escaping DefaultBoolResultAPISuccesClosure,
                             failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getStringResponseWith(route: URL,
                               success:@escaping DefaultStringResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getIntResponseWith(route: URL,
                            success:@escaping DefaultIntResultAPISuccesClosure,
                            failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
}
//MARK:- Post APIs
extension APIManagerBase{
    
    func postArrayResponseWith(route: URL,parameters: Parameters,
                               success:@escaping DefaultArrayResultAPISuccessClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? [AnyObject]{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? [AnyObject]{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postDictionaryResponseWith(route: URL,parameters: Parameters,
                                    success:@escaping DefaultAPISuccessClosure,
                                    failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
       
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
            
        }
    }
    
    func putDictionaryResponseWith(route: URL,parameters: Parameters,
                                       success:@escaping DefaultAPISuccessClosure,
                                       failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
          
           if withHeader{
               Alamofire.request(route, method: .put, parameters: parameters, encoding: URLEncoding(), headers: getAuthorizationHeader()).responseJSON{
                   response in
                   self.responseResult(response, success: {response in
                       if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                           success(dictionaryResponse)
                       }
                       else{
                           Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                       }
                   }, failure: {error in
                       failure(error as NSError)
                   })
               }
           }else {
               
               Alamofire.request(route, method: .put, parameters: parameters, encoding: URLEncoding()).responseJSON{
                   response in
                   self.responseResult(response, success: {response in
                       if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                           success(dictionaryResponse)
                       }
                       else{
                           success(Dictionary<String, AnyObject>())
                       }
                   }, failure: {error in
                       failure(error as NSError)
                   })
               }
               
           }
       }
    
    func postMultipartDictionaryResponseWith(route: URL,parameters: Parameters,
                                             success:@escaping DefaultAPISuccessClosure,
                                             failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                                success(dictionaryResponse)
                                            }
                                            else{
                                                Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                            }
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                    success(dictionaryResponse)
                                }
                                else{
                                    Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                }
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
    
    func postMultipartStringResponseWith(route: URL,parameters: Parameters,
                                             success:@escaping DefaultStringResultAPISuccesClosure,
                                             failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            if let stringResponse = result as? String{
                                                success(stringResponse)
                                            }
                                            else{
                                                Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                            }
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                if let stringResponse = result as? String{
                                    success(stringResponse)
                                }
                                else{
                                    Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                }
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
    
    
    func postBoolResponseWith(route: URL,parameters: Parameters,
                              success:@escaping DefaultBoolResultAPISuccesClosure,
                              failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postStringResponseWith(route: URL,parameters: Parameters,
                                success:@escaping DefaultStringResultAPISuccesClosure,
                                failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .post , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postIntResponseWith(route: URL,
                             success:@escaping DefaultIntResultAPISuccesClosure,
                             failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .post, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .post , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
}

public extension Data {
    public var mimeType:String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "image/jpeg";
            case 0x89:
                return "image/png";
            case 0x47:
                return "image/gif";
            case 0x49, 0x4D:
                return "image/tiff";
            case 0x25:
                return "application/pdf";
            case 0xD0:
                return "application/vnd";
            case 0x46:
                return "text/plain";
            default:
                print("mimeType for \(c[0]) in available");
                return "application/octet-stream";
            }
        }
    }
    public var getExtension: String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "_IMG.jpeg";
            case 0x89:
                return "_IMG.png";
            case 0x47:
                return "_IMG.gif";
            case 0x49, 0x4D:
                return "_IMG.tiff";
            case 0x25:
                return "_FILE.pdf";
            case 0xD0:
                return "_FILE.vnd";
            case 0x46:
                return "_FILE.txt";
            default:
                print("mimeType for \(c[0]) in available");
                return "_video.mp4";
            }
        }
    }
}
