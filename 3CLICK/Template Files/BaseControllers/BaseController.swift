
import Foundation
import UIKit
import LGSideMenuController

class BaseController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNotificationObserver()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigation()
    }
}
//MARK:- Add Navigation Items
extension BaseController{
    private func addBackBarButtonItem() {
        let image = UIImage(named: "back")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.onBtnBack))
        
        self.navigationItem.leftBarButtonItem = backItem
    }
    private func addSideMenuBarButtonItem() {
        let image = UIImage(named: "menuWhite")
        let sideMenuItem = UIBarButtonItem(image: image,
                                           style: .plain,
                                           target: self,
                                           action: #selector(self.onBtnSideMenu))
        
        self.navigationItem.leftBarButtonItem = sideMenuItem
    }
    private func addTruckLogo() {
        let image = UIImage(named: "truckLogo")
        let truckLogo = UIBarButtonItem(image: image,
                                        style: .plain,
                                        target: self,
                                        action: nil)
        self.navigationItem.rightBarButtonItem = truckLogo
    }
    func addNewCardButtonItem() {
        let image = UIImage(named: "add")
        let btnAddNewCard = UIBarButtonItem(image: image,
                                            style: .plain,
                                            target: self,
                                            action: #selector(self.pushToAddCardController))
        self.navigationItem.rightBarButtonItem = btnAddNewCard
    }
}
//MARK:- Selectors
extension BaseController{
    @objc func onBtnSideMenu(){
        sideMenuController?.showLeftViewAnimated()
    }
    @objc func onBtnBack(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func onBtnAddNewCard(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func notificationObserver(notification: NSNotification) {
        self.pushToEditProfile()
    }
}
//MARK:- Navigation Methods
extension BaseController{
    func pushToEditProfile(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "EditProfile")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToProfile(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Profile")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToChangePassword(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ChangePassword")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToSignUp(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "SignUp")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToLogin(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Login")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToForgotPassword(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ForgotPassword")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToFleets(hireFleetData:HireFleetData?){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Fleets") as! Fleets
        controller.hireFleetData = hireFleetData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToFleetDetails(fleet:FleetsModel,hireFleetData:HireFleetData?){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "FleetDetails") as! FleetDetails
        controller.fleet = fleet
        controller.hireFleetData = hireFleetData
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToRideDetails(ride:Bookings,rideDetail:RideDetailModel,rideType:BookingType){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "RideDetail") as! RideDetail
        controller.ride = ride
        controller.rideType = rideType
        controller.rideDetail = rideDetail
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToRiderNavigation(ride:RideDetailModel){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "RiderNavigation") as! RiderNavigation
        controller.rideDetail = ride
        controller.isDriverPlaced = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToAllCards(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AllCards") as! AllCards
        controller.shouldPopToHireBookings = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @objc func pushToAddCardController(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddCardController") as! AddCardController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func presentRateRide(rideDetail:RideDetailModel){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "RateRide") as! RateRide
        controller.rideDetail = rideDetail
        self.present(controller, animated: true, completion: nil)
    }
    func pushToCodeVerification(user:UserModel){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CodeVerification") as! CodeVerification
        controller.user = user
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToEmailVerification(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "EmailVerification")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToNewPassword(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "NewPassword")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToTermsAndConditions(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CMS") as! CMS
        controller.cmsType = .termsAndConditions
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func presentCancelRide(rideDetail:RideDetailModel){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CancelBooking") as! CancelBooking
        controller.rideDetail = rideDetail
        self.present(controller, animated: true, completion: nil)
    }
    func presentTransactinsDetail(selectedTransaction: TransactionModel){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "TransactionsDetail") as! TransactionsDetail
        controller.selectedTransaction = selectedTransaction
        self.present(controller, animated: true, completion: nil)
    }
}
//MARK:- Helper Methods
extension BaseController{
    private func addNotificationObserver(){
        let notificationName = Notification.Name("PushToEditProfile")
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationObserver), name: notificationName, object: nil)
    }
    private func resetNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = Global.APP_COLOR
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    func setNavigation(){
        let currentClassName = Utility.main.topViewController()?.className
        guard let navControllerCount = self.navigationController?.viewControllers.count else {return}
        if navControllerCount > 1 && AppStateManager.sharedInstance.isUserLoggedIn(){
            self.sideMenuController?.isLeftViewSwipeGestureEnabled = false
            self.navigationController?.navigationBar.isHidden = false
            self.addBackBarButtonItem()
        }
        else{
            self.navigationController?.navigationBar.isHidden = true
        }
        if currentClassName == "LGSideMenuController" && navControllerCount == 1{
            self.sideMenuController?.isLeftViewSwipeGestureEnabled = true
            self.navigationController?.navigationBar.isHidden = false
            self.addSideMenuBarButtonItem()
        }
        self.resetNavigationBar()
    }
    private func alignTitleToLeft(){
        let titleLabel = UILabel()
        titleLabel.text = self.navigationItem.title
        titleLabel.textColor = .white
        titleLabel.sizeToFit()
        let leftItem = UIBarButtonItem(customView: titleLabel)
        self.navigationItem.leftBarButtonItems?.append(leftItem)
        self.navigationItem.title = nil
    }
}
